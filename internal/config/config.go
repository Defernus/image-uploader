package config

import "os"

type Config struct {
	AwsAccessKeyId     string
	AwsSecretAccessKey string
	AwsEndpoint        string
	AwsRegion          string
}

func NewConfig() Config {
	awsAccessKey := os.Getenv("AWS_ACCESS_KEY_ID")
	awsSecretAccessKey := os.Getenv("AWS_SECRET_ACCESS_KEY")
	awsEndpoint := os.Getenv("AWS_ENDPOINT")
	awsRegion := os.Getenv("AWS_REGION")

	config := Config{
		AwsAccessKeyId:     awsAccessKey,
		AwsSecretAccessKey: awsSecretAccessKey,
		AwsEndpoint:        awsEndpoint,
		AwsRegion:          awsRegion,
	}

	return config
}
