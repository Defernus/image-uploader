package storage

import (
	"bytes"
	"io/ioutil"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type Storage struct {
	S3 *s3.S3
}

type FileData struct {
	Body        []byte
	ContentType string
}

func NewStorage(region, endpoint, accessKeyId, secretAccessKey string) (Storage, error) {
	s3Config := &aws.Config{
		Credentials:      credentials.NewStaticCredentials(accessKeyId, secretAccessKey, ""),
		Endpoint:         aws.String(endpoint),
		Region:           aws.String(region),
		DisableSSL:       aws.Bool(true),
		S3ForcePathStyle: aws.Bool(true),
	}
	sess, err := session.NewSession(s3Config)
	if err != nil {
		return Storage{}, err
	}

	svc := s3.New(sess)

	return Storage{
		S3: svc,
	}, nil
}

func (storage *Storage) CreateBucket(bucket string) error {
	cparams := &s3.CreateBucketInput{
		Bucket: aws.String(bucket),
	}
	if _, err := storage.S3.CreateBucket(cparams); err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case s3.ErrCodeBucketAlreadyExists:
				return aerr
			case s3.ErrCodeBucketAlreadyOwnedByYou:
				return nil
			default:
				return aerr
			}
		}
		return err
	}
	return nil
}

func (storage *Storage) UploadFile(bucket, key string, file FileData) error {
	object := &s3.PutObjectInput{
		Body:        bytes.NewReader(file.Body),
		ContentType: aws.String(file.ContentType),
		Bucket:      aws.String(bucket),
		Key:         aws.String(key),
	}

	if _, err := storage.S3.PutObject(object); err != nil {
		return err
	}
	return nil
}

func (storage *Storage) GetFile(bucket, key string) (FileData, error) {
	object := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}

	obj, err := storage.S3.GetObject(object)
	if err != nil {
		return FileData{}, err
	}
	defer obj.Body.Close()

	file, err := ioutil.ReadAll(obj.Body)
	if err != nil {
		return FileData{}, err
	}
	result := FileData{
		Body:        file,
		ContentType: *obj.ContentType,
	}
	return result, nil
}
func (storage *Storage) DeleteFile(bucket, key string) error {
	object := &s3.DeleteObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}

	if _, err := storage.S3.DeleteObject(object); err != nil {
		return err
	}
	return nil
}
