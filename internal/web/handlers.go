package web

import (
	"bytes"
	"errors"
	"fmt"
	"image"
	"io"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gorm.io/gorm"
	"imageapi.lavrentev.dev/rest/internal/database"
	"imageapi.lavrentev.dev/rest/internal/storage"
)

const (
	thumbnailPostfix = "thumbnail"
	thumbnailWidth   = 128
)

func (s *Server) handleGetImages(w http.ResponseWriter, r *http.Request) {
	images := []database.Image{}
	err := s.db.DB.Find(&images).Error
	if err != nil {
		InternalServerError(w)
		return
	}

	Success(w, images)
}

func (s *Server) handleAddImage(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseMultipartForm(32 << 20); err != nil {
		Failed(w, Error{
			StatusCode: http.StatusBadRequest,
			Message:    "too large file",
		})
		return
	}

	var buf bytes.Buffer
	file, _, err := r.FormFile("image")
	if err != nil {
		InternalServerError(w)
		return
	}
	defer file.Close()

	io.Copy(&buf, file)
	original := buf.Bytes()
	imgData, format, err := image.DecodeConfig(bytes.NewReader(original))
	if err != nil {
		Failed(w, Error{
			Message:    "wrong format",
			StatusCode: http.StatusBadRequest,
		})
		return
	}
	imageUUID := uuid.New().String()
	name := fmt.Sprintf("%v.%v", imageUUID, format)

	originalFile := storage.FileData{
		Body:        original,
		ContentType: fmt.Sprintf("image/%v", format),
	}
	if err := s.storage.UploadFile("images", name, originalFile); err != nil {
		InternalServerError(w)
		return
	}
	img := database.Image{
		Original: fmt.Sprintf("/images/%v", name),
	}

	if err := s.db.DB.Create(&img).Error; err != nil {
		InternalServerError(w)
		return
	}

	go func() {
		thumbnailHeight := uint(imgData.Height * thumbnailWidth / imgData.Width)
		thumbnail, err := createThumbnail(thumbnailWidth, thumbnailHeight, original)
		if err != nil {
			s.log.Errorf("failed to generate thumbnail: %v\n", err)
			return
		}
		thumbnailName := fmt.Sprintf("%v-%v.png", imageUUID, thumbnailPostfix)
		thumbnailFile := storage.FileData{
			Body:        thumbnail,
			ContentType: "image/png",
		}
		if err := s.storage.UploadFile("images", thumbnailName, thumbnailFile); err != nil {
			s.log.Errorf("failed to save file: %v\n", err)
			return
		}
		img.Thumbnail = fmt.Sprintf("/images/%v", thumbnailName)
		if err := s.db.DB.Save(&img).Error; err != nil {
			s.log.Errorf("failed to update image model: %v\n", err)
		}
	}()

	Success(w, img.ID)
}

// !TODO validate id
func (s *Server) handleDeleteImage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	img := database.Image{}
	err := s.db.DB.First(&img, vars["id"]).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		NotFound(w)
		return
	} else if err != nil {
		InternalServerError(w)
		return
	}
	originalBucket, originalKey, err := getBucketAndKey(img.Original)
	if err != nil {
		InternalServerError(w)
		return
	}
	if err := s.storage.DeleteFile(originalBucket, originalKey); err != nil {
		InternalServerError(w)
		return
	}
	thumbnailBucket, thumbnailKey, err := getBucketAndKey(img.Thumbnail)
	if err != nil {
		InternalServerError(w)
		return
	}
	if err := s.storage.DeleteFile(thumbnailBucket, thumbnailKey); err != nil {
		InternalServerError(w)
		return
	}

	if err := s.db.DB.Delete(&database.Image{}, vars["id"]).Error; err != nil {
		InternalServerError(w)
		return
	}

	Success(w, nil)
}

func (s *Server) handleGetImageData(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	img := database.Image{}
	err := s.db.DB.First(&img, vars["id"]).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		NotFound(w)
		return
	} else if err != nil {
		InternalServerError(w)
		return
	}

	Success(w, img)
}

func (s *Server) handleGetImage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	file, err := s.storage.GetFile("images", vars["key"])
	if err != nil {
		NotFound(w)
		return
	}

	w.Header().Set("Content-Type", file.ContentType)
	w.WriteHeader(http.StatusOK)
	w.Write(file.Body)
}
