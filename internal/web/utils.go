package web

import (
	"bytes"
	"fmt"
	"image"
	"image/png"
	"strings"

	"github.com/nfnt/resize"
)

func createThumbnail(width, height uint, original []byte) ([]byte, error) {
	img, _, err := image.Decode(bytes.NewReader(original))
	if err != nil {
		return nil, err
	}

	thumbnailImage := resize.Resize(width, height, img, resize.Bilinear)
	buff := new(bytes.Buffer)

	err = png.Encode(buff, thumbnailImage)
	if err != nil {
		return nil, err
	}

	return buff.Bytes(), nil
}

func getBucketAndKey(path string) (string, string, error) {
	pathData := strings.Split(path, "/")
	if len(pathData) != 3 {
		return "", "", fmt.Errorf("wrong path format")
	}
	return pathData[1], pathData[2], nil
}
