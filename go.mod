module imageapi.lavrentev.dev/rest

go 1.16

require (
	github.com/aws/aws-sdk-go v1.40.4
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.12
)
