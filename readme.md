# Simple gallery api

## TL;DR install
Create .env
```
AWS_ACCESS_KEY_ID=root
AWS_SECRET_ACCESS_KEY=qwertyuiop
AWS_ENDPOINT=http://minio:9000
AWS_REGION=us-east-1
MINIO_ROOT_USER=root
MINIO_ROOT_PASSWORD=qwertyuiop
```
Then run
```
docker-compose -f docker-compose.local.yml up
```

or try on https://imageapi.lavrentev.dev

## Docs

`{GET} /`  
get all images

`{GET} /:id`  
get image data by id

`{GET} /images/:key`  
get image itself by url

`{POST} /`  
form data with "image" field  
upload image

`{DELETE} /:id`  
delete image by id
